﻿window.onload = start;

var draw = true;
var hits = 1;

function start()
{
	setButtonToStart();
	setAlphabet();
}

function setButtonToStart()
{
	draw = true;
	document.getElementById("stop").innerHTML = '<div id="mainButton" onclick="setButtonToStop();">'+"Click to play!"+'</div>';
}

function setButtonToStop()
{
	setAlphabet();
	document.getElementById("stop").innerHTML = '<div id="mainButton" onclick="stop();">'+"STOP"+'</div>';
	document.getElementById("gamble").style.color = "#CCCCCC";
	document.getElementById("info").innerHTML = 'Click button above to stop draw.';
	document.getElementById("score").innerHTML = '';
	drawNumber();
	hits = 1;
}

function stop()
{
	draw = false;
	document.getElementById("info").innerHTML = "Select one number";
}

function drawNumber()
{
	drawnNumber = Math.floor(Math.random()*35);
	document.getElementById("gamble").innerHTML = drawnNumber;
	if (draw == true) setTimeout("drawNumber()",70);
	else
	{
	document.getElementById("gamble").innerHTML = "?";
	document.getElementById("gamble").style.color = "rgb(160,0,0)";
	}
}

function setAlphabet()
{
	var divy = "";
	var element;
	for (i = 0; i < 35; i++) 
	{
		element = "ele" + i;
		divy = divy + '<div class="button" id="'+element+'" onclick="check('+i+');">'+i+'</div>';
	}
	document.getElementById("alp").innerHTML = divy;
}

function check(i)
{
	if (draw == false)
	{
	element = "ele" + i;
	document.getElementById(element).innerHTML = "";
	
		if (i > drawnNumber) 
		{
		document.getElementById("info").innerHTML = "Drawn number is SMALLER.";
		hits++;
		}
		else if (i < drawnNumber) 
		{	
		document.getElementById("info").innerHTML = "Drawn number is BIGGER.";
		hits++;
		}
		else if (i == drawnNumber) 
		{	
		var score;
		
		switch (hits)
		{
		case 1:
			score = "You are the master!";
			break;
		case 2:
			score = "Great!";
			break;
		case 3:
			score = "Very well!";
			break;
		case 4:
			score = "Good!";
			break;
		case 5:
			score = "Ok!";
			break;
		case 6:
			score = "Not bad!";
			break;
		default:
			score = "You have to practise more!";
		}
		document.getElementById("info").innerHTML = "CONGRATULATIONS! YOU HAVE WON 1000$!";
		document.getElementById("score").innerHTML = "" +score+ " Your score is: "+ hits + " hit(s).";
		document.getElementById("gamble").innerHTML = drawnNumber;
		document.getElementById("gamble").style.color = "rgb(20,0, 200)";
		setButtonToStart();
		}
	}
}



